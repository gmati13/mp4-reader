## mp4-reader

Simple mp4 file reader that returns metadata of the file.


Walker provide several commands to navigate through tree and show properties.

You can navigate through all nodes marked `object`

Supported walker commands:
```bash
show # show all properties
show <property name> # show selected property
cd <path> # navigate through metadata tree
exit # exit from app
```