#include <iostream>
#include "videoreader/VideoReader.h"
#include "walker/Walker.h"

using namespace std;

int main() {
    const string video_file = "/home/mati/projects/clion/test/assets/file_example_MP4_480_1_5MG.mp4";

    VideoReader reader(video_file);

    reader.read_metadata();

    Walker walker(reader.get_metadata());

//    walker.run();

    walker.do_command("cd /moov/trak/mdia/hdlr");

//    walker.run();
    cout << walker.get_path_string() << ":" << endl;
    walker.do_command("show");

    return 0;
}