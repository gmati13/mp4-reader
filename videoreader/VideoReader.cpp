//
// Created by mati on 16/12/2020.
//

#include "iostream"
#include "VideoReader.h"
#include "instructions/instructions.h"

#include <utility>

VideoReader::VideoReader(string video_path) {
    path = move(video_path);

    stream = new ifstream();
    metadata = new Metadata();

    Context* context = new Context();

    factory = new Factory<Context>(context);
    context->factory = factory;
    context->metadata = metadata;
    context->stream = stream;

    factory->set_instruction("/ftyp", read_ftyp);
    factory->set_instruction("/moov", read_moov);
    factory->set_instruction("/moov/mvhd", read_mvhd);
//    factory->set_instruction("clip", read_clip);
//    factory->set_instruction("crgn", read_crgn);
    factory->set_instruction("/moov/iods", skip);
    factory->set_instruction("/moov/trak", read_trak);
    factory->set_instruction("/moov/trak/tkhd", read_tkhd);
    factory->set_instruction("/moov/trak/edts", read_edts);
    factory->set_instruction("/moov/trak/edts/elst", read_elst);
    factory->set_instruction("/moov/trak/mdia", read_mdia);
    factory->set_instruction("/moov/trak/mdia/mdhd", read_mdhd);
    factory->set_instruction("/moov/trak/mdia/hdlr", read_hdlr);
}

void VideoReader::read_metadata() {
    stream->open(path);

    uint32_t offset;
    uint32_t size = 0;
    string section;
    ParsedSegment segment;
    vector<string> current_path;

    do {
        offset = (int)stream->tellg() - 4;
        segment = read_segment<uint32_t>(*stream);
        section = segment.reversed_string;

        string instruction;
        for (const auto& path_section: current_path) {
            instruction += "/" + path_section;
        }
        instruction += "/" + section;

        if (factory->has_instruction(instruction)) {
            current_path.emplace_back(section);

            if (factory->ctx->current == nullptr) {
                factory->ctx->current = &(*metadata)[section];
                factory->ctx->current->parent = metadata;
            } else {
                Metadata* current = factory->ctx->current;
                factory->ctx->current = &(*factory->ctx->current)[section];
                factory->ctx->current->parent = current;
            }

            factory->ctx->current->path = current_path;
            (*factory->ctx->current)["size"].value = to_string(size);
            (*factory->ctx->current)["offset"].value = to_string(offset);

            factory->run_instruction(instruction);

            while (
                stream->tellg() == offset + size
                && !current_path.empty()
                && factory->ctx->current != metadata
            ) {
                current_path.pop_back();
                factory->ctx->current = factory->ctx->current->parent;
                if (factory->ctx->current != metadata) {
                    offset = atoi((*factory->ctx->current)["offset"].value.c_str());
                    size = atoi((*factory->ctx->current)["size"].value.c_str());
                }
            }

            size = 0;
        } else if (size) {
            cout << "unknown instruction for \"" << instruction << "\"" << endl;
            return;
        } else {
            size = segment.reversed;
        }
    } while (stream->good());
}

string VideoReader::get_path() {
    return path;
}

Metadata* VideoReader::get_metadata() {
    return metadata;
}

float to_fixed_point_number(uint32_t number) {
    return ( number >> 16 ) + ( number & 0xffff ) / 65536.0;
}

float to_fixed_point_number16(uint16_t number) {
    return ( number >> 8 ) + ( number & 0xff ) / 256.0;
}

void skip_bytes(ifstream& stream, uint64_t bytes) {
    char *t = new char [bytes];
    stream.read(t, bytes);
}

string read_string(ifstream& stream) {
    string result;
    getline(stream, result, '\0');
    return result;
}