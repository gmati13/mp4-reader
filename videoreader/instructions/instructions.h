//
// Created by mati on 17/12/2020.
//

#ifndef TEST_INSTRUCTIONS_H
#define TEST_INSTRUCTIONS_H

#include "../VideoReader.h"
#include "iostream"

using namespace std;

void read_ftyp(Context*);
void read_moov(Context*);
void read_mvhd(Context*);
void read_clip(Context*);
void read_crgn(Context*);
void read_trak(Context*);
void read_tkhd(Context*);
void read_edts(Context*);
void read_elst(Context*);
void read_mdia(Context*);
void read_mdhd(Context*);
void read_hdlr(Context*);

void skip(Context*);

#endif //TEST_INSTRUCTIONS_H
