//
// Created by mati on 17/12/2020.
//

#include "instructions.h"

void read_ftyp(Context* ctx) {
    Metadata* meta = ctx->current;

    (*meta)["major_brand"].value = read_segment<uint32_t>(*ctx->stream).reversed_string;
    (*meta)["major_brand_version"].value = to_string(read_segment<uint32_t>(*ctx->stream).reversed);

    while (ctx->stream->tellg() < atoi((*meta)["size"].value.c_str())) {
        (*meta)["compatible_brands"].value += read_segment<uint32_t>(*ctx->stream).reversed_string + " ";
    }
}