//
// Created by mati on 20/12/2020.
//

#include "instructions.h"

void read_elst(Context* ctx) {
    Metadata* meta = ctx->current;

    ParsedSegment segment = read_segment<uint32_t>(*ctx->stream);
    (*meta)["version"].value = to_string(segment.reversed >> 24);
    (*meta)["flags"].value = to_string(segment.reversed & 0xffffff);

    (*meta)["number_of_edits"].value = to_string(read_segment<uint32_t>(*ctx->stream).reversed);

    if ((*meta)["version"].value == "0") {
        (*meta)["time_length"].value = to_string(read_segment<uint32_t>(*ctx->stream).reversed);
        (*meta)["start_time"].value = to_string(read_segment<uint32_t>(*ctx->stream).reversed);
    } else {
        (*meta)["time_length"].value = to_string(read_segment<uint64_t>(*ctx->stream).reversed);
        (*meta)["start_time"].value = to_string(read_segment<uint64_t>(*ctx->stream).reversed);
    }

    (*meta)["playback_speed"].value =
        to_string(to_fixed_point_number(read_segment<uint32_t>(*ctx->stream).reversed));
}