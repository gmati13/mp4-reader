//
// Created by mati on 17/12/2020.
//

#include "instructions.h"

void read_crgn(Context* ctx) {
    Metadata* meta = ctx->current;

    (*meta)["region_size"].value = to_string(read_segment<uint16_t>(*ctx->stream).reversed);
    (*meta)["region_boundary_x"].value = to_string(read_segment<uint32_t>(*ctx->stream).reversed);
    (*meta)["region_boundaty_y"].value = to_string(read_segment<uint32_t>(*ctx->stream).reversed);
}