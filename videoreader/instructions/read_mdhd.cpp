//
// Created by mati on 20/12/2020.
//

#include "instructions.h"

void read_mdhd(Context* ctx) {
    Metadata* meta = ctx->current;

    ParsedSegment segment = read_segment<uint32_t>(*ctx->stream);
    (*meta)["version"].value = to_string(segment.reversed >> 24);
    (*meta)["flags"].value = to_string(segment.reversed & 0xffffff);

    if ((*meta)["version"].value == "0") {
        (*meta)["created"].value = to_string(read_segment<uint32_t>(*ctx->stream).reversed);
        (*meta)["modified"].value = to_string(read_segment<uint32_t>(*ctx->stream).reversed);
    } else {
        (*meta)["created"].value = to_string(read_segment<uint64_t>(*ctx->stream).reversed);
        (*meta)["modified"].value = to_string(read_segment<uint64_t>(*ctx->stream).reversed);
    }

    (*meta)["time_scale"].value = to_string(read_segment<uint32_t>(*ctx->stream).reversed);

    if ((*meta)["version"].value == "0") {
        (*meta)["duration"].value = to_string(read_segment<uint32_t>(*ctx->stream).reversed);
    } else {
        (*meta)["duration"].value = to_string(read_segment<uint64_t>(*ctx->stream).reversed);
    }

    (*meta)["language"].value = to_string(read_segment<uint16_t>(*ctx->stream).reversed);
    (*meta)["quicktime_quality"].value = to_string(read_segment<uint16_t>(*ctx->stream).reversed);
}