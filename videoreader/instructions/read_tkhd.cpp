//
// Created by mati on 18/12/2020.
//

#include "instructions.h"

void read_tkhd(Context* ctx) {
    Metadata* meta = ctx->current;

    ParsedSegment segment = read_segment<uint32_t>(*ctx->stream);
    (*meta)["version"].value = to_string(segment.reversed >> 24);
    (*meta)["flags"].value = to_string(segment.reversed & 0xffffff);

    if ((*meta)["version"].value == "0") {
        (*meta)["created"].value = to_string(read_segment<uint32_t>(*ctx->stream).reversed);
        (*meta)["modified"].value = to_string(read_segment<uint32_t>(*ctx->stream).reversed);
    } else {
        (*meta)["created"].value = to_string(read_segment<uint64_t>(*ctx->stream).reversed);
        (*meta)["modified"].value = to_string(read_segment<uint64_t>(*ctx->stream).reversed);
    }

    (*meta)["track_id"].value = to_string(read_segment<uint32_t>(*ctx->stream).reversed);
    skip_bytes(*ctx->stream, 8);

    if ((*meta)["version"].value == "0") {
        (*meta)["duration"].value = to_string(read_segment<uint32_t>(*ctx->stream).reversed);
    } else {
        (*meta)["duration"].value = to_string(read_segment<uint64_t>(*ctx->stream).reversed);
    }

    skip_bytes(*ctx->stream, 4);

    (*meta)["video_layer"].value = to_string(read_segment<uint16_t>(*ctx->stream).reversed);
    (*meta)["quicktime_alternate/other"].value = to_string(read_segment<uint16_t>(*ctx->stream).reversed);

    (*meta)["track_audio_volume"].value =
        to_string(to_fixed_point_number16(read_segment<uint16_t>(*ctx->stream).reversed));

    skip_bytes(*ctx->stream, 2);

    string window_geometry[9] {
            "video_geometry_width_scale",
            "video_geometry_width_rotate",
            "video_geometry_width_angle",
            "video_geometry_height_rotate",
            "video_geometry_height_scale",
            "video_geometry_height_angle",
            "video_geometry_position_left",
            "video_geometry_position_top",
            "video_geometry_divider_scale"
    };

    for (const string& prop: window_geometry) {
        (*meta)[prop].value = to_string(to_fixed_point_number(read_segment<uint32_t>(*ctx->stream).reversed));
    }

    (*meta)["frame_size_width"].value =
            to_string(to_fixed_point_number(read_segment<uint32_t>(*ctx->stream).reversed));
    (*meta)["frame_size_height"].value =
            to_string(to_fixed_point_number(read_segment<uint32_t>(*ctx->stream).reversed));
}