//
// Created by mati on 20/12/2020.
//

#include "instructions.h"

void read_hdlr(Context* ctx) {
    Metadata* meta = ctx->current;

    ParsedSegment segment = read_segment<uint32_t>(*ctx->stream);
    (*meta)["version"].value = to_string(segment.reversed >> 24);
    (*meta)["flags"].value = to_string(segment.reversed & 0xffffff);

    (*meta)["quicktime_type"].value = read_segment<uint32_t>(*ctx->stream).reversed_string;
    (*meta)["subtype/media_type"].value = read_segment<uint32_t>(*ctx->stream).reversed_string;
    (*meta)["quicktime_manufacturer"].value = read_segment<uint32_t>(*ctx->stream).reversed_string;
    (*meta)["quicktime_component_flags"].value = to_string(read_segment<uint32_t>(*ctx->stream).reversed);
    (*meta)["quicktime_component_flags_mask"].value = to_string(read_segment<uint32_t>(*ctx->stream).reversed);
    (*meta)["component_type_name"].value = read_string(*ctx->stream);
//    cout << ctx->stream->tellg() << endl;
//    skip(ctx);
//    cout << ctx->stream->tellg() << endl;
}