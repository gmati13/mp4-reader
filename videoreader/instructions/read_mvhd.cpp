//
// Created by mati on 17/12/2020.
//

#include "instructions.h"

void read_mvhd(Context* ctx) {
    Metadata* meta = ctx->current;

    ParsedSegment segment = read_segment<uint32_t>(*ctx->stream);
    (*meta)["version"].value = to_string(segment.reversed >> 24);
    (*meta)["flags"].value = to_string(segment.reversed & 0xffffff);


    if ((*meta)["version"].value == "0") {
        (*meta)["created"].value = to_string(read_segment<uint32_t>(*ctx->stream).reversed);
        (*meta)["modified"].value = to_string(read_segment<uint32_t>(*ctx->stream).reversed);
    } else {
        (*meta)["created"].value = to_string(read_segment<uint64_t>(*ctx->stream).reversed);
        (*meta)["modified"].value = to_string(read_segment<uint64_t>(*ctx->stream).reversed);
    }

    (*meta)["time_scale"].value = to_string(read_segment<uint32_t>(*ctx->stream).reversed);

    if ((*meta)["version"].value == "0") {
        (*meta)["duration"].value = to_string(read_segment<uint32_t>(*ctx->stream).reversed);
    } else {
        (*meta)["duration"].value = to_string(read_segment<uint64_t>(*ctx->stream).reversed);
    }

    (*meta)["user_playback_speed"].value =
        to_string(to_fixed_point_number(read_segment<uint32_t>(*ctx->stream).reversed));
    (*meta)["user_volume"].value = to_string(to_fixed_point_number16(read_segment<uint16_t>(*ctx->stream).reversed));

    skip_bytes(*ctx->stream, 10);

    string window_geometry[9] {
        "window_geometry_width_scale",
        "window_geometry_width_rotate",
        "window_geometry_width_angle",
        "window_geometry_height_rotate",
        "window_geometry_height_scale",
        "window_geometry_height_angle",
        "window_geometry_position_left",
        "window_geometry_position_top",
        "window_geometry_divider_scale"
    };

    for (const string& prop: window_geometry) {
        (*meta)[prop].value = to_string(to_fixed_point_number(read_segment<uint32_t>(*ctx->stream).reversed));
    }

    (*meta)["quicktime_preview_start_time"].value = to_string(read_segment<uint32_t>(*ctx->stream).reversed);
    (*meta)["quicktime_preview_time_length"].value = to_string(read_segment<uint32_t>(*ctx->stream).reversed);
    (*meta)["quicktime_still_poster"].value = to_string(read_segment<uint32_t>(*ctx->stream).reversed);
    (*meta)["quicktime_selection_start_time"].value = to_string(read_segment<uint32_t>(*ctx->stream).reversed);
    (*meta)["quicktime_selection_time_length"].value = to_string(read_segment<uint32_t>(*ctx->stream).reversed);

    (*meta)["quicktime_current_time"].value = to_string(read_segment<uint32_t>(*ctx->stream).reversed);
    (*meta)["next_track_id"].value = to_string(read_segment<uint32_t>(*ctx->stream).reversed);

}