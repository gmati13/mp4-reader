//
// Created by mati on 18/12/2020.
//

#include "instructions.h"

void skip(Context* ctx) {
    skip_bytes(*ctx->stream,
               atoi((*ctx->current)["size"].value.c_str())
               + atoi((*ctx->current)["offset"].value.c_str())
               - ctx->stream->tellg());
}