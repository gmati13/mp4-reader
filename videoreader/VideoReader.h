//
// Created by mati on 16/12/2020.
//

#ifndef TEST_VIDEOREADER_H
#define TEST_VIDEOREADER_H

#include "fstream"
#include "string"
#include "map"
#include "vector"
#include "../factory/Factory.h"

using namespace std;

struct ParsedSegment {
    uint64_t original;
    uint64_t reversed;
    string reversed_string;
};

struct Metadata {
    Metadata* parent;
    string value;
    vector<string> path;
    Metadata& operator [](const string& key) {
        return (Metadata &) children[key];
    }
    map<string, Metadata> get_children() {
        return children;
    }

private:
    map<string, Metadata> children;
};

struct Context {
    Metadata* metadata;
    ifstream* stream;
    Factory<Context>* factory;
    Metadata* current;
};

class VideoReader {
public:
    explicit VideoReader(string);

    string get_path();
    Metadata* get_metadata();

    void read_metadata();
private:
    string path;
    Metadata* metadata;
    ifstream* stream;
    Factory<Context>* factory;
};

template <typename T>
string parse_segment_to_string(T segment) {
    string result;
    for (int i = 0; i < sizeof(T); i++) {
        result += (char)(segment >> ((sizeof(T) - i - 1) * 8) & 0xff);
    }

    return result;
}

template <typename T>
T reverse_segment(T x) {
    T out = 0, i;
    for(i = 0; i < sizeof(T); ++i)
    {
        const T byte = (x >> 8 * i) & 0xff;
        out |= byte << (((sizeof(T)-1)*8) - 8 * i);
    }
    return out;
}

template <typename T>
ParsedSegment read_segment(ifstream& stream) {
    T segment = 0;
    stream.read((char*)&segment, sizeof(T));
    T reversed = reverse_segment<T>(segment);

    return ParsedSegment{segment, reversed, parse_segment_to_string<T>(reversed)};
}



float to_fixed_point_number(uint32_t);
float to_fixed_point_number16(uint16_t);
void skip_bytes(ifstream&, uint64_t);
string read_string(ifstream&);


#endif //TEST_VIDEOREADER_H
