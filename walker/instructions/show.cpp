//
// Created by mati on 18/12/2020.
//

#include "instructions.h"

void show(WalkerContext* ctx) {
    function<void(pair<string, Metadata>)> print = [](pair<string, Metadata> meta) {
        cout << "\t" << meta.first;

        if (meta.second.value.length()) {
            cout << ": " << meta.second.value;
        } else if (!meta.second.get_children().empty()) {
            cout << " (object)";
        } else {
            cout << " (empty)";
        }

        cout << endl;
    };

    if (ctx->arguments.empty()) {
        for (auto meta: ctx->current->get_children()) {
            print(meta);
        }
    } else if (ctx->current->get_children().find(ctx->arguments[0]) != ctx->current->get_children().end()) {
        pair<string, Metadata> meta { ctx->arguments[0],(*ctx->current)[ctx->arguments[0]] };
        print(meta);
    }

}