//
// Created by mati on 18/12/2020.
//

#include "instructions.h"

void cd(WalkerContext* ctx) {
    if (ctx->arguments.empty()) {
        cout << "argument is required" << endl;
        return;
    }

    vector<string> paths;

    size_t pos = 0;
    string token;

    while ((pos = ctx->arguments[0].find((string)"/")) != string::npos) {
        token = ctx->arguments[0].substr(0, pos);
        ctx->arguments[0].erase(0, pos + 1);

        if (!token.empty()) {
            paths.emplace_back(token);
        }
    }
    paths.emplace_back(ctx->arguments[0]);

    for (const string& path: paths) {
        if (path == ".." && ctx->current != ctx->metadata) {
            ctx->path.pop_back();
            ctx->current = ctx->parent;
            if (ctx->current->parent != nullptr) {
                ctx->parent = ctx->current->parent;
            } else {
                ctx->parent = ctx->metadata;
            }
        } else if (ctx->current->get_children().find(path) != ctx->current->get_children().end()) {
            ctx->path.emplace_back(path);
            ctx->parent = ctx->current;
            ctx->current = &(*ctx->current)[path];
        }
    }
}