//
// Created by mati on 18/12/2020.
//

#ifndef TEST_INSTRUCTIONS_H
#define TEST_INSTRUCTIONS_H

#include "../Walker.h"

void show(WalkerContext*);
void cd(WalkerContext*);

#endif //TEST_INSTRUCTIONS_H
