//
// Created by mati on 18/12/2020.
//

#ifndef TEST_WALKER_H
#define TEST_WALKER_H

#include "../videoreader/VideoReader.h"

struct WalkerContext {
    Metadata* parent;
    Metadata* metadata;
    Metadata* current;
    vector<string> arguments;
    vector<string> path;
};

class Walker {
public:
    explicit Walker(Metadata* metadata);

    void set_prompt(string);
    void run();
    void do_command(string);
    string get_path_string();
private:
    WalkerContext* context;
    Factory<WalkerContext>* factory;
    string prompt;
};


#endif //TEST_WALKER_H
