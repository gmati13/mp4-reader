//
// Created by mati on 18/12/2020.
//

#include "Walker.h"

#include <utility>
#include "instructions/instructions.h"



Walker::Walker(Metadata* metadata) {
    context = new WalkerContext();

    context->parent = nullptr;
    context->metadata = metadata;
    context->current = metadata;

    factory = new Factory<WalkerContext>(context);

    factory->set_instruction("show", show);
    factory->set_instruction("cd", cd);
}

void Walker::run() {
    set_prompt("");
    string command_string;

    do {
        cout << prompt << ": ";
        getline(cin, command_string);

        if (command_string == "exit") {
            break;
        }

        do_command(command_string);
    } while (true);

    cout << "goodbye!" << endl;
}

void Walker::set_prompt(string next_prompt) {
    prompt = move(next_prompt);
}

void Walker::do_command(string command_string) {
    string command;
    vector<string> arguments;
    command = "";
    arguments.clear();
    size_t pos = 0;
    string token;

    while ((pos = command_string.find((string)" ")) != string::npos) {
        token = command_string.substr(0, pos);
        command_string.erase(0, pos + 1);

        if (command.empty()) {
            command = token;
        } else {
            arguments.emplace_back(token);
        }
    }

    if (command.empty()) {
        command = command_string;
    } else {
        arguments.emplace_back(command_string);
    }

    if (factory->has_instruction(command)) {
        context->arguments = arguments;

        factory->run_instruction(command);
    } else {
        cout << "unknown command \"" << command << "\"" << endl;
    }

    set_prompt(get_path_string());
}

string Walker::get_path_string() {
    string path_string;
    for (auto path: context->path) {
        path_string += (string)"/" + path;
    }
    return path_string;
}
