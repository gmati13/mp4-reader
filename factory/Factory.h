//
// Created by mati on 16/12/2020.
//

#ifndef TEST_FACTORY_H
#define TEST_FACTORY_H

#include "functional"
#include "string"
#include "map"
#include "iostream"

using namespace std;

template <typename T>
class Factory {
public:
    T* ctx;

    explicit Factory(T* context);

    function<void(T*)> get_instruction(string name);
    void set_instruction(string name, function<void(T*)> instruction);
    void run_instruction(string name);
    bool has_instruction(string name);
private:
    map<string, function<void(T*)>> instructions;
};

template<typename T>
Factory<T>::Factory(T* context) {
    ctx = context;
}

template<typename T>
void Factory<T>::run_instruction(string name) {
    if (instructions.find(name) != instructions.end()) {
        instructions[name](ctx);
    } else {
        throw runtime_error("class Factory: unknown instruction \"" + name + "\"");
    }
}

template<typename T>
function<void(T *)> Factory<T>::get_instruction(string name) {
    return instructions[name];
}

template<typename T>
void Factory<T>::set_instruction(string name, function<void(T *)> instruction) {
    instructions[name] = instruction;
}

template<typename T>
bool Factory<T>::has_instruction(string name) {
    instructions.find(name) != instructions.end();
}


#endif //TEST_FACTORY_H
